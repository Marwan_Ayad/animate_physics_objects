﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class Record_Anime : MonoBehaviour {
	public Transform Target_to_Animate;
	public int totalFrames;
	public AnimationClip anim_Clip;
	public float Counter=0f;
	public float Record_After_Sec=0.2f;
	List<GameObject> mylist = new List<GameObject>();
	GameObject[] CHields ;
	AnimationCurve xAxic = new AnimationCurve();
	AnimationCurve yAxic = new AnimationCurve();
	AnimationCurve zAxic = new AnimationCurve();
	AnimationCurve xrot = new AnimationCurve();
	AnimationCurve yrot = new AnimationCurve();
	AnimationCurve zrot = new AnimationCurve();
	AnimationCurve wrot = new AnimationCurve();
	Record_Anime source;
	void Start () {
//		anim_Clip = Resources.Load ("Test") as AnimationClip;
		anim_Clip.ClearCurves ();
		anim_Clip.legacy = true;
		anim_Clip.frameRate = 30f;
		source = transform.root.GetComponent <Record_Anime> ();

		foreach (Transform item in transform) {
			for (int i = 0; i < item.childCount; i++) {
				if (item.GetChild(i).GetComponent<Rigidbody>() != null) {
					#if UNITY_EDITOR
					UnityEditorInternal.ComponentUtility.CopyComponent (source);
					UnityEditorInternal.ComponentUtility.PasteComponentAsNew (item.GetChild(i).gameObject);
					#endif
				}
			}
		}

		Target_to_Animate = this.transform.root;
	}
	

	void FixedUpdate () {
		if (Time.time > Counter) 
		{
			xAxic.AddKey (Counter, transform.position.x);
			yAxic.AddKey (Counter, transform.position.y);
			zAxic.AddKey (Counter, transform.position.z);
			xrot.AddKey (Counter, transform.rotation.x);
			yrot.AddKey (Counter, transform.rotation.y);
			zrot.AddKey (Counter, transform.rotation.z);
			wrot.AddKey (Counter, transform.rotation.w);

			anim_Clip.SetCurve (AnimationUtility.CalculateTransformPath (transform, Target_to_Animate), typeof(Transform), "localPosition.x", xAxic);
			anim_Clip.SetCurve (AnimationUtility.CalculateTransformPath (transform, Target_to_Animate), typeof(Transform), "localPosition.y", yAxic);
			anim_Clip.SetCurve (AnimationUtility.CalculateTransformPath (transform, Target_to_Animate), typeof(Transform), "localPosition.z", zAxic);
			anim_Clip.SetCurve (AnimationUtility.CalculateTransformPath (transform, Target_to_Animate), typeof(Transform), "m_LocalRotation.x", xrot);
			anim_Clip.SetCurve (AnimationUtility.CalculateTransformPath (transform, Target_to_Animate), typeof(Transform), "m_LocalRotation.y", yrot);
			anim_Clip.SetCurve (AnimationUtility.CalculateTransformPath (transform, Target_to_Animate), typeof(Transform), "m_LocalRotation.z", zrot);
			anim_Clip.SetCurve (AnimationUtility.CalculateTransformPath (transform, Target_to_Animate), typeof(Transform), "m_LocalRotation.w", wrot);
			anim_Clip.EnsureQuaternionContinuity ();
			Counter += Record_After_Sec;
		}
	}
		

}
